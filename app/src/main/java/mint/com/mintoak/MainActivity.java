package mint.com.mintoak;

import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.freshchat.consumer.sdk.Freshchat;
import com.freshchat.consumer.sdk.FreshchatConfig;
import com.freshchat.consumer.sdk.FreshchatUser;

public class MainActivity extends AppCompatActivity
{
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    String restoreId;
    FloatingActionButton fab_convo;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
        //init
        FreshchatConfig freshchatConfig=new FreshchatConfig("7548b35d-5d50-4189-ae36-5c3ced583988","bc65c219-2b5c-4136-b5b8-9f4969091e40");
        freshchatConfig.setCameraCaptureEnabled(true);
        freshchatConfig.setGallerySelectionEnabled(true);
        Freshchat.getInstance(getApplicationContext()).init(freshchatConfig);
        //Update user information
        FreshchatUser user = Freshchat.getInstance(getApplicationContext()).getUser();
        user.setFirstName(Constants.F_NAME).setLastName(Constants.L_NAME).setEmail(Constants.EMAIL);
        Freshchat.getInstance(getApplicationContext()).setUser(user);
        Freshchat.getInstance(getApplicationContext()).identifyUser(Constants.EMAIL, "");
    }

    private void init()
    {
        sharedPreferences=getSharedPreferences("user",0);
        editor=sharedPreferences.edit();
        fab_convo=findViewById(R.id.fab_convo);
        fab_convo.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                String ar[]={"Old Conversation","New Conversation"};
                AlertDialog.Builder mBuilder = new AlertDialog.Builder(MainActivity.this);
                mBuilder.setTitle("Choose an Conversation");
                mBuilder.setItems(ar, new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i)
                    {
                        if (i==0)
                        {
                            restoreId = Freshchat.getInstance(getApplicationContext()).getUser().getRestoreId();
                            Freshchat.getInstance(getApplicationContext()).identifyUser(Constants.EMAIL, restoreId);
                            Freshchat.showConversations(MainActivity.this);
                        }
                        if (i==1)
                        {
                            Freshchat.resetUser(getApplicationContext());
                            Freshchat.getInstance(getApplicationContext()).identifyUser(Constants.EMAIL, "");
                            saveRestoreIdForUser(restoreId);
                            Freshchat.showConversations(MainActivity.this);
                        }
                    }
                });
                AlertDialog mDialog = mBuilder.create();
                mDialog.show();

            }
        });
    }

    private void saveRestoreIdForUser(String restoreId)
    {
        editor.putString("restoreId",restoreId);
        editor.commit();
    }
}
